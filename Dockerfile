FROM golang:1.17 as builder

WORKDIR /app

COPY hello.go /app

RUN go build /app/hello.go  

FROM scratch

COPY --from=builder /app/hello /

ENTRYPOINT ["./hello"]